const router = require("express").Router();
const co = require("co");
const mongodb = require("mongodb");
const cedric = require("cedric/build/index");
const { find } = require("lodash");

const input = {
  vendorName: "tjetak",
  boardInput: [
    {
      length: 55,
      width: 38,
      height: 28.6,
      substance: "K125/M125/K125",
      flute: "B/F",
      quantity: 3000,
      layout: "B1",
      discountSheet: -0.05,
      vendorMargin: 0.75,
      tjetakMargin: 0.05,
      net: {
        length: 190,
        width: 66.6,
      },
      ink: {
        type: "full-block",
        amount: 1,
      },
      print: {
        type: "full-block",
      },
      pond: {
        type: "Viking",
        length: 1408,
      },
      finishing: {
        glue: true,
      },
      additionalFlute: [],
    },
  ],
};

router.get("/", (req, res) => {
  co(function* fetchData() {
    const client = yield mongodb.MongoClient.connect(process.env.DB_FLANSBURG);
    let docs;

    if (
      input["vendorName"].toUpperCase().includes("TJETAK") ||
      input["vendorName"].toUpperCase().includes("CALCULATOR")
    ) {
      docs = yield client
        .db("flansburg")
        .collection("corrugateds")
        .find()
        .toArray();
    } else {
      docs = yield client
        .db("flansburg")
        .collection("corrugateds")
        .find({ vendor_id: input["vendorName"] })
        .toArray();
    }
    const inputan = input["boardInput"];
    return calculate(docs, inputan);
  }).catch((error) => {
    res.send("Error: " + error.toString());
  });

  function calculate(docs, inputan) {
    totalVendor = docs.length;
    let vendor = [];
    let result = [];

    const changedInput = JSON.parse(JSON.stringify(inputan));

    for (let i = 0; i < totalVendor; i++) {
      const corrugatedData = docs[i]["corrugated"];
      vendor.push(docs[i]["vendor_id"]);

      if (inputan.length > 0) {
        corrugatedData["singleFace"].forEach((element) => {
          corrugatedData["boards"].push(element);
        });
        cedric.boards = corrugatedData["boards"];
        cedric.print = corrugatedData["print"];
        cedric.finishing = corrugatedData["finishing"];
        cedric.pond = corrugatedData["pond"];
        cedric.pondBoard = corrugatedData["pondBoard"];
        cedric.runningMeter = corrugatedData["minRunningMeter"];
        cedric.minOrderQty = corrugatedData["minSheetQty"];
        cedric.minTransactionValue = corrugatedData["minTransactionValue"];
        cedric.middleLayer = corrugatedData["middle"];

        for (let j = 0; j < inputan.length; j++) {
          const checkLayout = find(corrugatedData["layouts"], {
            layout_type: inputan[j].layout,
          });
          const P = inputan[j].length;
          const L = inputan[j].width;
          const T = inputan[j].height;

          if (
            changedInput[j].layout !== "CUSTOM" ||
            inputan[j].layout !== "CUSTOM"
          ) {
            // lebar sheet actual
            const actual = eval(checkLayout.actual_width);
            changedInput[j].net.width = actual;

            // panjang beli sheet
            const length_buy = eval(checkLayout.order_length);
            changedInput[j].net.length = length_buy;
          }

          const suggestions = cedric.calculate([changedInput[j]])[0];
          suggestions[0]["vendor_id"] = vendor[i];
          result.push(suggestions[0]);
        }
      }
    }
    res.send(JSON.stringify(result));
  }
});

module.exports = router;
