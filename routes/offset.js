const router = require("express").Router();
const co = require("co");
const mongodb = require("mongodb");
const oscar = require("oscar-v2/build/index");
const { updateDuplexPrice } = require("../utils/utils");

const input = {
  vendorName: "tjetak",
  paperInput: [
    {
      finishing: {},
      colors: { front: 1, back: 1 },
      width: 980,
      length: 700,
      quantity: 3400,
      uniform: false,
      type: "Duplex 310 GSM APKI +370%",
      name: "Kertas",
      _id: "kin7ocwm",
      apkiPercentage: "370",
      upLayout: 1,
      sheets: 1,
      specialColors: { back: 0, front: 0 },
      includePrintPrice: true,
    },
  ],
  boardInput: [],
  addOnInput: [],
  spongeInput: [],
  maxSuggestion: 3,
};

router.get("/", (req, res) => {
  co(function* fetchData() {
    const client = yield mongodb.MongoClient.connect(process.env.DB_FLANSBURG);
    let docs;

    if (
      typeof input["vendorName"] === "object" &&
      input["vendorName"].length > 0
    ) {
      docs = yield client
        .db("flansburg")
        .collection("offsets")
        .find({ vendor_id: { $in: input["vendorName"] } })
        .toArray();
    } else {
      if (
        input["vendorName"].toUpperCase().includes("TJETAK") ||
        input["vendorName"].toUpperCase().includes("CALCULATOR")
      ) {
        docs = yield client
          .db("flansburg")
          .collection("offsets")
          .find({ vendor_id: { $not: /.*corrugated.*/ } })
          .toArray();
      } else {
        docs = yield client
          .db("flansburg")
          .collection("offsets")
          .find({ vendor_id: input["vendorName"] })
          .toArray();
      }
    }

    return calculate(docs, input);
  }).catch((error) => {
    res.send("Error: " + error.toString());
  });
  function calculate(docs, req) {
    totalVendor = docs.length;
    let vendor = [];

    let paperResult = [];
    let boardResult = [];
    let hardboxAddOnResult = [];
    let spongeResult = [];

    let paperResult_2 = [];
    let boardResult_2 = [];
    let hardboxAddOnResult_2 = [];
    let spongeResult_2 = [];

    let paperResult_3 = [];
    let boardResult_3 = [];
    let hardboxAddOnResult_3 = [];
    let spongeResult_3 = [];

    let paperResult_4 = [];
    let boardResult_4 = [];
    let hardboxAddOnResult_4 = [];
    let spongeResult_4 = [];

    for (let i = 0; i < totalVendor; i++) {
      let finishing = {};
      const offsetData = docs[i]["offset"];
      vendor.push(docs[i]["vendor_id"]);
      let papersInput;
      let persistentPaperInput;

      if (req["paperInput"].length > 0) {
        for (let j = 0; j < req["paperInput"].length; j++) {
          if (req["paperInput"][j].finishing.cut && !finishing.cut) {
            finishing.cut = offsetData["finishing"]["cut"];
          }
          if (req["paperInput"][j].finishing.emboss && !finishing.emboss) {
            finishing.emboss = offsetData["finishing"]["emboss"];
          }
          if (req["paperInput"][j].finishing.poly && !finishing.poly) {
            finishing.poly = offsetData["finishing"]["poly"];
          }
          if (req["paperInput"][j].finishing.glue && !finishing.glue) {
            finishing.glue = offsetData["finishing"]["glue"];
          }
          if (
            req["paperInput"][j].finishing.bottomPaperbagGlue &&
            !finishing.bottomPaperbagGlue
          ) {
            finishing.bottomPaperbagGlue =
              offsetData["finishing"]["bottomPaperbagGlue"];
          }
          if (
            req["paperInput"][j].finishing.sidePaperbagGlue &&
            !finishing.sidePaperbagGlue
          ) {
            finishing.sidePaperbagGlue =
              offsetData["finishing"]["sidePaperbagGlue"];
          }
          if (req["paperInput"][j].finishing.fold && !finishing.fold) {
            finishing.fold = offsetData["finishing"]["fold"];
          }
          if (req["paperInput"][j].finishing.eyelet && !finishing.eyelet) {
            finishing.eyelet = offsetData["finishing"]["eyelet"];
          }
          if (
            req["paperInput"][j].finishing.metalClamp &&
            !finishing.metalClamp
          ) {
            finishing.metalClamp = offsetData["finishing"]["metalClamp"];
          }
          if (req["paperInput"][j].finishing.strap && !finishing.strap) {
            finishing.strap = offsetData["finishing"]["strap"];
          }
          if (req["paperInput"][j].finishing.yarn && !finishing.yarn) {
            finishing.yarn = offsetData["finishing"]["yarn"];
          }
          if (
            req["paperInput"][j].finishing.lamination &&
            !finishing.lamination
          ) {
            finishing.lamination = offsetData["finishing"]["lamination"];
          }
          if (req["paperInput"][j].finishing.spotUv && !finishing.spotUv) {
            finishing.spotUv = offsetData["finishing"]["spotUv"];
          }
          if (req["paperInput"][j].finishing.pond && !finishing.pond) {
            finishing.pond = offsetData["finishing"]["pond"];
          }
          if (
            req["paperInput"][j].finishing.numerator &&
            !finishing.numerator
          ) {
            finishing.numerator = offsetData["finishing"]["numerator"];
          }
          if (req["paperInput"][j].finishing.spiral && !finishing.spiral) {
            finishing.spiral = offsetData["finishing"]["spiral"];
          }
          if (req["paperInput"][j].finishing.hotGlue && !finishing.hotGlue) {
            finishing.hotGlue = offsetData["finishing"]["hotGlue"];
          }
          if (req["paperInput"][j].finishing.wire && !finishing.wire) {
            finishing.wire = offsetData["finishing"]["wire"];
          }
        }

        oscar.machines = offsetData["machines"];

        papersInput = JSON.parse(JSON.stringify(req["paperInput"])); // make req['paperInput'] copy, passed by value
        let [papersData, pInput] = updateDuplexPrice(
          offsetData["papers"],
          papersInput
        );
        persistentPaperInput = pInput;

        oscar.papers = papersData;
        oscar.finishing = finishing;
      }
      if (req["addOnInput"].length > 0) {
        let addOnData = {};
        req["addOnInput"].forEach((addOn) => {
          if (addOn.tape) {
            addOnData["tape"] = offsetData["hardboxAddOn"]["tape"];
          }
          if (addOn.magnet) {
            addOnData["magnet"] = offsetData["hardboxAddOn"]["magnet"];
          }
          if (addOn.partitionGlue) {
            addOnData["partitionGlue"] =
              offsetData["hardboxAddOn"]["partitionGlue"];
          }
          if (addOn.glue) {
            addOnData["glue"] = offsetData["hardboxAddOn"]["glue"];
          }
        });
        oscar.hardboxAddOn = addOnData;
      }
      const pondFinishing = {
        pond: offsetData["boardFinishing"]["boardPond"],
      };
      if (req["boardInput"].length > 0) {
        oscar.boards = offsetData["boards"];
        oscar.boardFinishing = pondFinishing;
      }
      if (req["spongeInput"].length > 0) {
        oscar.sponges = offsetData["sponge"];
        oscar.boardFinishing = pondFinishing;
      }

      if (docs[i]["vendor_id"] !== "tjetak") req.maxSuggestion = 1;
      const suggestions = oscar.calculateSuggestions(
        persistentPaperInput,
        req.boardInput,
        req.addOnInput,
        req.spongeInput,
        req.maxSuggestion
      );
      for (let k = 0; k < suggestions.length; k++) {
        arr = [
          paperResult,
          boardResult,
          hardboxAddOnResult,
          spongeResult,
          paperResult_2,
          boardResult_2,
          hardboxAddOnResult_2,
          spongeResult_2,
          paperResult_3,
          boardResult_3,
          hardboxAddOnResult_3,
          spongeResult_3,
        ];
        suggestions[k].map((s) => {
          s["vendor_id"] = vendor[i];
          arr[k].push(s);
        });
      }
    }
    let finalRes = [];
    finalRes.push(paperResult);
    finalRes.push(boardResult);
    finalRes.push(hardboxAddOnResult);
    finalRes.push(spongeResult);
    res.send(JSON.stringify(finalRes));
  }
});

module.exports = router;
