const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongodb = require('mongodb');
const postRoute = require('./routes/offset');
const cedric = require('./routes/corrugated');

dotenv.config();

//connect to DB
mongodb.MongoClient.connect(
  process.env.DB_CONNECTION,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => {
    console.log('connected to db!');
  }
);

//Middleware
app.use(express.json());

// Route Middleware
app.use('/api/offset', postRoute);
app.use('/api/cedric', cedric);

app.listen(5000, () => console.log('Server up and running!'));
