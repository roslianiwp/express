module.exports = {
  updateDuplexPrice: (papers, papersInput) => {
    const duplex = "DUPLEX";
    const divisorToDecimalPercent = 100;
    const lengthDivisor = 1000;
    const grammageDivisor = 1000;
    if (papersInput.length > 0) {
      papersInput.forEach((paperInput) => {
        if (
          paperInput.type.toUpperCase().includes(duplex) &&
          paperInput.apkiPercentage > 0
        ) {
          const newDuplexPapers = papers.filter((p) =>
            p.name.includes(paperInput.type.split("APKI")[0])
          );

          if (newDuplexPapers.length > 0) {
            newDuplexPapers.forEach((newDuplexPaper) => {
              let newPaperName = newDuplexPaper.name;
              let apkiPricePerKg = newDuplexPaper.price_per_kg; // already include apki
              let pricePerSheet = newDuplexPaper.price_per_sheet;
              const grammage = newDuplexPaper.grammage;
              const length = newDuplexPaper.length;
              const width = newDuplexPaper.width;
              const apkiPercentage = parseInt(
                newDuplexPaper.name.split("APKI +")[1]
              );

              if (apkiPercentage != paperInput.apkiPercentage) {
                newPaperName =
                  newDuplexPaper.name.split(" APKI ")[0] +
                  " APKI +" +
                  paperInput.apkiPercentage +
                  "%";

                // calculate base price based on previous apki and price per kg
                const pricePerKgBeforeApki = Math.floor(
                  newDuplexPaper.price_per_kg /
                    (1 + apkiPercentage / divisorToDecimalPercent)
                );
                // calculate apki price based on base price and new apki percentage
                apkiPricePerKg =
                  pricePerKgBeforeApki +
                  (paperInput.apkiPercentage / divisorToDecimalPercent) *
                    pricePerKgBeforeApki;
                pricePerSheet =
                  (length / lengthDivisor) *
                  (width / lengthDivisor) *
                  (grammage / grammageDivisor) *
                  apkiPricePerKg;
              }

              const newDuplex = {
                name: newPaperName,
                grammage: grammage,
                length: length,
                width: width,
                price_per_kg: apkiPricePerKg,
                price_per_sheet: pricePerSheet,
              };

              paperInput.type = newPaperName;
              papers.push(newDuplex);
            });
          }
        }
      });
    }
    return [papers, papersInput];
  },
};
